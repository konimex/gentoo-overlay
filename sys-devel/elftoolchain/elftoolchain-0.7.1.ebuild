# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI=5
inherit eutils

DESCRIPTION="BSD Toolchain"
HOMEPAGE="https://elftoolchain.sourceforge.net"
SRC_URI="mirror://sourceforge/elftoolchain/${P}.tar.gz"
LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64"


DEPEND="sys-devel/bmake
        sys-devel/bison
        app-arch/sharutils
        app-arch/libarchive
        sys-devel/gcc
        sys-libs/zlib
        dev-python/pyyaml"

src_prepare(){
	rm -rf test
	rm -rf documentation
}

src_compile() {
	bmake
}

src_install() {
	bmake DESTDIR="${D}" install
}
